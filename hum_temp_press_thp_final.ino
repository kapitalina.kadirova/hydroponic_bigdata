/*slo-nodemcu05*/
//Das Originalprogramm ist unter  http://internetofthinking.blogspot.de/2016/02/upload-ds18b20-temperature-sensor-data.html beschrieben und wurde hier nur angepasst
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <OneWire.h>
#include "bsec.h"
#define myPeriodic 15 //in sec | Thingspeak pub is 15sec

//--------zum HC-SR04
#define echopin 14 // echo pin
#define trigpin 12 // Trigger pin

// Helper functions declarations
void checkIaqSensorStatus(void);
void errLeds(void);

// Create an object of the class Bsec
Bsec iaqSensor;
String output;

// Achtung hier Eingabe
const char* MY_SSID = ""; // Eingabe Ihres WLAN-SSID GOLDENGATE
const char* MY_PWD = "";// Eingabe Ihres WLAN-Passworts 

String WriteAPIKey ="";// Eingabe des WriteAPI-Key
String feldnummer="1";// Eingabe der Feldnummer
String feldnummer2="2";// Eingabe der Feldnummer
String feldnummer3="5";// Eingabe der Feldnummer
// Ende der Eingabe

const char* server = "api.thingspeak.com";

int sent = 0;
int sent2 = 0;
int sent3 = 0;

void setup(void) {
  Serial.begin(115200);
  connectWifi();
   //-----------------zum HC-SR04
  pinMode (trigpin, OUTPUT);
  pinMode (echopin, INPUT );
  //-----------------------------------------
    iaqSensor.begin(BME680_I2C_ADDR_PRIMARY, Wire);
//  output = "\nBSEC library version " + String(iaqSensor.version.major) + "." + String(iaqSensor.version.minor) + "." + String(iaqSensor.version.major_bugfix) + "." + String(iaqSensor.version.minor_bugfix);
//  Serial.println(output);
  checkIaqSensorStatus();

  bsec_virtual_sensor_t sensorList[10] = {
    BSEC_OUTPUT_RAW_TEMPERATURE,
    BSEC_OUTPUT_RAW_PRESSURE,
    BSEC_OUTPUT_RAW_HUMIDITY,
    BSEC_OUTPUT_RAW_GAS,
    BSEC_OUTPUT_IAQ,
    BSEC_OUTPUT_STATIC_IAQ,
    BSEC_OUTPUT_CO2_EQUIVALENT,
    BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
  };

  iaqSensor.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_LP);
  checkIaqSensorStatus();

---------------------------------------  
}
void loop(void) {
  float temp;
  float hum;
  float co2;
  float pres;
  float duration, distance, volume;
   unsigned long time_trigger = millis();
  if (iaqSensor.run()) { // If new data is available
    output = String(time_trigger);
    output += ", " + String(iaqSensor.temperature);
    output += ", " + String(iaqSensor.humidity);
    output += ", " + String(iaqSensor.co2Equivalent);
    output += ", " + String(iaqSensor.pressure);

    Serial.println(output);
  } else {
    checkIaqSensorStatus();
  }
     //-----------------zum HC-SR04
    digitalWrite(trigpin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigpin, HIGH);
    delayMicroseconds(10);
    duration = pulseIn (echopin, HIGH);
    distance = duration / 58.2;
    volume = (23 - distance) * 593.95; // V in cm3
    //delay (500);
    Serial.println(distance);
    sendDistance(distance);
       //-----------------zum HC-SR04
    
  temp = iaqSensor.temperature;
  hum = iaqSensor.humidity;
  co2 = iaqSensor.co2Equivalent;
  pres = iaqSensor.pressure;
  sendHumidityTS(hum);
  sendTeperatureTS(temp);
  sendPressureTS(pres);
  int count = myPeriodic;
  while(count--)
  //String tempC = dtostrf(temp, 4, 1, buffer);//handled in sendTemp()
  Serial.print(String(sent)+" Temperature: ");
  Serial.println(temp);  
  Serial.print(String(sent2)+" Humidity: ");
  Serial.println(hum); 
  Serial.print(String(sent2)+" Co2Equivalent: ");
  Serial.println(co2); 
  Serial.print(String(sent2)+" Pressure: ");
  Serial.println(pres); 
 }
void connectWifi()
{
  Serial.print("Connecting to "+*MY_SSID);
  WiFi.begin(MY_SSID, MY_PWD);
  while (WiFi.status() != WL_CONNECTED) {
  delay(1000);
  Serial.print(".");
  }  
  Serial.println("");
  Serial.println("Connected");
  Serial.println("");  
}//end connect

void sendDistance(float distance)
{  
   WiFiClient client3;
  
   if (client3.connect(server, 80)) { // use ip 184.106.153.149 or api.thingspeak.com
   Serial.println("WiFi Client connected ");
   
   String postStr = WriteAPIKey;
//  postStr += "&field1=";
    postStr += "&field";
    postStr += String(feldnummer3);
    postStr += "=";
    postStr += String(distance);
    postStr += "\r\n\r\n";

   
   client3.print("POST /update HTTP/1.1\n");
   client3.print("Host: api.thingspeak.com\n");
   client3.print("Connection: close\n");
   client3.print("X-THINGSPEAKAPIKEY: " + WriteAPIKey + "\n");
   client3.print("Content-Type: application/x-www-form-urlencoded\n");
   client3.print("Content-Length: ");
   client3.print(postStr.length());
   client3.print("\n\n");
   client3.print(postStr);
   Serial.println("gesendet");
   delay(1000);
   
   }//end if
   sent3++;
 client3.stop();
}//end send

void sendTeperatureTS(float temp)
{  
   WiFiClient client;
  
   if (client.connect(server, 80)) { // use ip 184.106.153.149 or api.thingspeak.com
   Serial.println("WiFi Client connected ");
   
   String postStr = WriteAPIKey;
//  postStr += "&field1=";
    postStr += "&field";
    postStr += String(feldnummer);
    postStr += "=";
    postStr += String(temp);
    postStr += "\r\n\r\n";

   
   client.print("POST /update HTTP/1.1\n");
   client.print("Host: api.thingspeak.com\n");
   client.print("Connection: close\n");
   client.print("X-THINGSPEAKAPIKEY: " + WriteAPIKey + "\n");
   client.print("Content-Type: application/x-www-form-urlencoded\n");
   client.print("Content-Length: ");
   client.print(postStr.length());
   client.print("\n\n");
   client.print(postStr);
   delay(1000);
   
   }//end if
   sent++;
 client.stop();
}//end send
void sendHumidityTS(float hum)
{  
   WiFiClient client2;
  
   if (client2.connect(server, 80)) { // use ip 184.106.153.149 or api.thingspeak.com
   Serial.println("WiFi Client connected ");
   
   String postStr = WriteAPIKey;
//  postStr += "&field1=";
    postStr += "&field";
    postStr += String(feldnummer2);
    postStr += "=";
    postStr += String(hum);
    postStr += "\r\n\r\n";

   
   client2.print("POST /update HTTP/1.1\n");
   client2.print("Host: api.thingspeak.com\n");
   client2.print("Connection: close\n");
   client2.print("X-THINGSPEAKAPIKEY: " + WriteAPIKey + "\n");
   client2.print("Content-Type: application/x-www-form-urlencoded\n");
   client2.print("Content-Length: ");
   client2.print(postStr.length());
   client2.print("\n\n");
   client2.print(postStr);
   delay(1000);
   
   }//end if
   sent2++;
 client2.stop();
}//end send
void sendPressureTS(float pres)
{  
   WiFiClient client3;
  
   if (client3.connect(server, 80)) { // use ip 184.106.153.149 or api.thingspeak.com
   Serial.println("WiFi Client connected ");
   
   String postStr = WriteAPIKey;
//  postStr += "&field1=";
    postStr += "&field";
    postStr += String(feldnummer3);
    postStr += "=";
    postStr += String(pres);
    postStr += "\r\n\r\n";

   
   client3.print("POST /update HTTP/1.1\n");
   client3.print("Host: api.thingspeak.com\n");
   client3.print("Connection: close\n");
   client3.print("X-THINGSPEAKAPIKEY: " + WriteAPIKey + "\n");
   client3.print("Content-Type: application/x-www-form-urlencoded\n");
   client3.print("Content-Length: ");
   client3.print(postStr.length());
   client3.print("\n\n");
   client3.print(postStr);
   delay(1000);
   
   }//end if
   sent2++;
 client3.stop();
}//end send
//-------------------------------------------------------------------
void checkIaqSensorStatus(void)
{
  if (iaqSensor.status != BSEC_OK) {
    if (iaqSensor.status < BSEC_OK) {
      output = "BSEC error code : " + String(iaqSensor.status);
      Serial.println(output);
      for (;;)
        errLeds(); /* Halt in case of failure */
    } else {
      output = "BSEC warning code : " + String(iaqSensor.status);
      Serial.println(output);
    }
  }

  if (iaqSensor.bme680Status != BME680_OK) {
    if (iaqSensor.bme680Status < BME680_OK) {
      output = "BME680 error code : " + String(iaqSensor.bme680Status);
      Serial.println(output);
      for (;;)
        errLeds(); /* Halt in case of failure */
    } else {
      output = "BME680 warning code : " + String(iaqSensor.bme680Status);
      Serial.println(output);
    }
  }
}


void errLeds(void)
{
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
}
