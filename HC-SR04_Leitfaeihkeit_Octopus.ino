/*
  HC-SR04 Ping distance sensor:
  VCC to arduino 5v
  GND to arduino GND
  Echo to Arduino pin 9
  Trig to Arduino pin 8*/
#include <Wire.h>
#include <ESP8266WiFi.h>
#define myPeriodic 15 //in sec | Thingspeak pub is 15sec

#define echopin 14 // echo pin
#define trigpin 12 // Trigger pin

//Achtung hier Eingabe
const char* MY_SSID = ""; // Eingabe Ihres WLAN-SSID "lina"
const char* MY_PWD = "";// Eingabe Ihres WLAN-Passworts "passwort"

String WriteAPIKey ="";// Eingabe des WriteAPI-Key
String feldnummer3="3";// Eingabe der Feldnummer
String feldnummer4="4";
//String feldnummer3="Field 3";
// Ende der Eingabe

const char* server = "api.thingspeak.com";
float prevTemp = 0;
int sent = 0;
int sent2 = 0;

const int analogInPin = A0;
int leitfaeihkeit;

void setup() {
  Serial.begin(115200);
  connectWifi();
  pinMode (trigpin, OUTPUT);
  pinMode (echopin, INPUT );
}

void loop ()
{
  {
    float duration, distance, volume;
    digitalWrite(trigpin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigpin, HIGH);
    delayMicroseconds(10);
    duration = pulseIn (echopin, HIGH);
    distance = duration / 58.2;
    volume = ((22.06 - distance) * 522.79)/1000; // V in Liter
    delay (50);
    Serial.println(distance);
    Serial.print("Volume = ");
    Serial.println(volume);
    sendVolume(volume);
    leitfaeihkeit = analogRead(analogInPin);
  Serial.print("Analog sensor = ");
  Serial.println(leitfaeihkeit);
  sendLeitfaeihkeit(leitfaeihkeit);
  //sendData(volume, leitfaeihkeit);
  delay(5000);
  }
}

void connectWifi()
{
  Serial.print("Connecting to "+*MY_SSID);
  WiFi.begin(MY_SSID, MY_PWD);
  while (WiFi.status() != WL_CONNECTED) {
  delay(1000);
  Serial.print(".");
  }  
  Serial.println("");
  Serial.println("Connected");
  Serial.println("");  
}//end connect

void sendVolume(float volume)
{  
   WiFiClient client;
  
   if (client.connect(server, 80)) { // use ip 184.106.153.149 or api.thingspeak.com
   Serial.println("WiFi Client connected ");
   
   String postStr = WriteAPIKey;
//  postStr += "&field1=";
    postStr += "&field";
    postStr += String(feldnummer3);
    postStr += "=";
    postStr += String(volume);
    postStr += "\r\n\r\n";

   
   client.print("POST /update HTTP/1.1\n");
   client.print("Host: api.thingspeak.com\n");
   client.print("Connection: close\n");
   client.print("X-THINGSPEAKAPIKEY: " + WriteAPIKey + "\n");
   client.print("Content-Type: application/x-www-form-urlencoded\n");
   client.print("Content-Length: ");
   client.print(postStr.length());
   client.print("\n\n");
   client.print(postStr);
   Serial.println("gesendet");
   delay(1000);
   
   }//end if
   sent++;
 client.stop();
}//end send

void sendLeitfaeihkeit(float leitfaeihkeit)
{  
   WiFiClient client2;
  
   if (client2.connect(server, 80)) { // use ip 184.106.153.149 or api.thingspeak.com
   Serial.println("WiFi Client connected ");
   
   String postStr2 = WriteAPIKey;
//  postStr2 += "&field1=";
    postStr2 += "&field";
    postStr2 += String(feldnummer4);
    postStr2 += "=";
    postStr2 += String(leitfaeihkeit);
    postStr2 += "\r\n\r\n";

   client2.print("POST /update HTTP/1.1\n");
   client2.print("Host: api.thingspeak.com\n");
   client2.print("Connection: close\n");
   client2.print("X-THINGSPEAKAPIKEY: " + WriteAPIKey + "\n");
   client2.print("Content-Type: application/x-www-form-urlencoded\n");
   client2.print("Content-Length: ");
   client2.print(postStr2.length());
   client2.print("\n\n");
   client2.print(postStr2);
   Serial.println("gesendet 2");
   delay(1000);   
   }//end if
   sent2++;
 client2.stop();
}//end send
